#!/usr/bin/env python3
"""Streaming Media Downloader"""
import time
import re
from locust.clients import HttpSession

class StreamDownloader(object):
    """Streaming Media Downloader"""

    def __init__(self, client, media_path, auth):
        """Init"""
        self.client = client
        self.media_path = media_path
        self.auth = auth

    def stream(self, media_file):
        """Run"""
        print("Downloading m3u8 file {0}".format(media_file))
        time.sleep(1) # locust bug? need to sleep or requsts show as 0
        content = self.client.get(self.media_path + media_file, auth = self.auth)
        time.sleep(1)
        lines = content.iter_lines()

        # Check the start tag
        if (lines.next() != "#EXTM3U"):
            raise ValueError("Unexpected start tag")
        
        # Get the chunk durations
        durline = lines.next()
        match = re.search(r"#EXT-X-TARGETDURATION:(\d+)", durline)
        if (match == None):
            raise ValueError("Failed to parse duration line")
        duration = float(match.group(1))
        print("Duration is {0}s per chunk".format(duration))

        # Filter out comments
        chunks = [x for x in lines if x[0] != '#']

        print("Reading first 3 chunks")
        for chunk in chunks[0:3]:
            self.download_chunk(chunk)

        print("Streaming {0} chunks".format(len(chunks)-3))
        for chunk in chunks[3:]:
            time.sleep(duration)
            self.download_chunk(chunk)

        print("Done")

    def download_chunk(self, file_name):
        """Download One Chunk"""
        resp = self.client.get(self.media_path + file_name, auth = self.auth)
        resp.raise_for_status()
        print resp

if __name__ == "__main__":
    session = HttpSession(base_url="http://rillcontentdev.turner.com")
    downloader = StreamDownloader(session, "/content/589/video/segment/", ("rillservice", "key2thedoor"))
    downloader.stream("30_589-medium.m3u8")


