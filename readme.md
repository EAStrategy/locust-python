
## Locust Streaming Media Load Tester

Locust python script that reads an m3u8 and downloads the chunks, simulating a streaming media client.

## Setup

    $ pip install locustio

## Run

To run locally, execute

    $ locust -f run.py -H http://rillcontentdev.turner.com

To run in a distributed setup, use the run_master.sh and run_worker.sh

