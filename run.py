#!/usr/bin/env python3
"""Locust Streaming Media"""
from locust import Locust, TaskSet, task
import downloader

class WebsiteTasks(TaskSet):
    """Web Site Tasks"""
    
    @task
    def run(self):
        """Get the m3u8 file"""
        dl = downloader.StreamDownloader(self.client, "/content/589/video/segment/", ("rillservice", "key2thedoor"))
        dl.stream("30_589-medium.m3u8")

class WebsiteUser(Locust):
    """Web Site Config"""
    task_set = WebsiteTasks
    min_wait = 5000
    max_wait = 15000
